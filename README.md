# Robocode

### O que é Robocode?

O [**Robocode**](https://robocode.sourceforge.io/) é um jogo de programação, onde o objetivo é desenvolver um robô tank de guerra para batalhar contra outros robôs em **java** ou **.NET**.

### Descrição do Robô

Fiquei muito perdido no começo, e gastei praticamente um dia pra entender melhor como tudo estava funcionando, meu robô tem algumas funcionalidades interessantes porem simples, nada demais para dois dias, entre elas estão, uma função que detecta se o robô está próximo das paredes, e dessa forma ele tenta evitá-las. Ele possui duas funções relacionadas com os tiros, uma verifica a distância entre meu robô e o inimigo e assim determina qual poder de fogo usar contra ele, sendo o disparo mais forte quando os robôs estão proximos, sendo mais difícil de errar. A outra função mede quanto de energia o inimigo tem e se ele tiver uma baixa quantidade de energia é realizado um tiro fatal para matar o robô adversário. Além disso meu robô tem um sistema de mira básico porém a arma aponta pro último lugar onde viu o inimigo, e assim chama as funções de tiro. Se meu robô estiver perto do inimigo ele vai fazer uma movimentação circular. Um dos pontos fracos do meu robô é a longa distância, por isso fiz com que ele escaneasse sua volta e fosse atrás do primeiro robô que visse. Não é perfeito mas consegui diversas vítórias contra robôs mais fracos do robocode. Me diverti bastante no processo e fiquei bem feliz em conseguir um resultado relativamente aceitável.

### Processo de Produção

Fiz esse projeto para uma vaga na **Solutis**, infelizmente tive pouco tempo para desenvolve-lo, apenas 2 dias de código no total. Eu não tinha nenhuma experiência com **Java** ou **.NET** e nem com o **Robocode**, por isso foi um desafio em tanto, o processo de estudo da física do jogo foi muito interessante e o **Robocode** é definitivamente muito mais complexo e interessante do que eu imaginava. Tive muita dificuldade em usar algumas funções da API e obtive muitos erros no compilador, queria muito ter feito um trabalho melhor e estudar a fundo as funcionalidades do **Robocode,** acabei pesquisando em diversos sites e usei muito a **Wiki do Robocode** para adicionar implementações no meu projeto. Editei todo o código no próprio editor do **Robocode** e foi bem chato a parte de organização, mas acredito ter feito um bom trabalho em tão pouco tempo.

### Conclusão

Aprendi muito fazendo esse projeto e pretendo continuar estudando o **Robocode** quando estiver estudando **java** ou **.NET.** Contudo, foi muito difícil de conseguir implementar os comandos por ser algo totalmente diferente pra mim, mas eu fiz o que pude. Este robô pode ser baixado e clonado por qualquer pessoa que queira usar/estudar o código e fazer suas próprias alterações.