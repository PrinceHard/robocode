package RobotTet;
import robocode.*;
import robocode.ScannedRobotEvent;
import java.awt.Color;

 //Tet - a robot by PrinceHard

public class Tet extends AdvancedRobot{
	int turnDirection = 1;
	public void run() {
	
		setColors(Color.cyan,Color.gray,Color.gray); // corpo,arma ,radar
		setBulletColor(new Color(255, 255, 100));
        setScanColor(new Color(0, 255, 0));
		
		setAdjustGunForRobotTurn(true);// Define a arma para girar independente da virada do robô 
		
		//Rodando a arma a procura de inimigos		
		while(true) {
			turnGunRight(360 * turnDirection);
		
		//Chamando a função pertoParede e evitando a colisão
		pertoParede();
		if(pertoParede()) {
			ahead(100);
		}
	}		
}
		
	public void onScannedRobot(ScannedRobotEvent e) {

	//Aponta para onde viu o inimigo pela última vez.
	double absoluteBearing = getHeadingRadians () + e.getBearingRadians ();
		setTurnGunRightRadians (
    	robocode.util.Utils.normalRelativeAngle (absoluteBearing - 
        	getGunHeadingRadians ()));
			fogoInteligente(e.getDistance());//Chamando a função fogoInteligente
			//Verifica se é necessário dar o tiroFatal
			if(e.getEnergy() < 12){
				tiroFatal(e.getEnergy());
			}
			
			//Analisa o ambiente e vai na direção do inimigo
			if(e.getBearing() >= 0) {
				turnDirection = 1;
			} else {
				turnDirection = -1;
			}
				fogoInteligente(e.getDistance());//Chamando a função fogoInteligente
				turnRight(e.getBearing());
				ahead(e.getDistance() - 100);
				scan();
				
			//Se a distancia entre você e o robo inimigo for curta faça movimentos em circulos
			if(e.getDistance() <= 280){
				setAhead(100);
				setTurnLeft(90);
				execute();
			}	
	}
		

	public void onHitByBullet(HitByBulletEvent e) {
	}
	

	public void onHitWall(HitWallEvent e) {
	}
	

	//Retorna um valor positivo se estiver perto da parede
	public boolean pertoParede() {
		return (getX() < 50 || getX() > getBattleFieldWidth() - 50 ||
				getY() < 50 || getY() > getBattleFieldHeight() - 50);
	}
	

	//Poder de fogo variando a depender da distancia do inimigo
	public void fogoInteligente(double distanciaRobo){
		if (distanciaRobo > 400 || getEnergy() < 15){
			fire(1);
		}else if (distanciaRobo > 250){
			fire(2);
		}else {
			fire(3);
		}
	}


	//Tiro baseado na energia do inimigo
	public void tiroFatal(double energiaInimigo) {
		double tiro = (energiaInimigo / 4) + .1;
			fire(tiro);
	}
	

	//Comemoração de vitória
	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
}
